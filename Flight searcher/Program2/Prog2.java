/*************************************************************************/
//		By Zachary Holland
//		CS330 Program2
//		Description
//			- This program will process the query information given the
//			- entered flight number from the user. It will then look up
//			- the city name and country name for the given airport code,
//			- as well as calculate the number of other flights available, 
//			- and the distance between the origin and destination of the
//			- flight number.
/*************************************************************************/
import java.sql.*;
import java.io.*;
import java.lang.NullPointerException;
import java.util.Scanner;

public class Prog2 {
	static String flightNumberInput = null;

	//Radius of Earth obtained from google
	static Double earthRadius = 6335.44;
////////////////////////////////////////////////////////////////////////////////
// deg2rad
//  -This function converts decimal degrees to radians             
	
	private static double deg2rad(double deg) {
	  return (deg * Math.PI / 180.0);
	}

////////////////////////////////////////////////////////////////////////////////
// rad2deg
//  -This function converts radians to decimal degrees             

	private static double rad2deg(double rad) {
	  return (rad * 180 / Math.PI);
	}
////////////////////////////////////////////////////////////////////////////////
// getFlightNumber
// -Get the flight number from the user
	public static String getFlightNumber(){
		String enteredFlightNumb = "";
		//Get users input and process the flight information request
    	try{
			System.out.print("Enter flight number: ");
	    	BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    	enteredFlightNumb = bufferRead.readLine();
	    	
	    }catch(IOException e){
			System.out.println("Seems you entered something bad, try again.");
		}
		return enteredFlightNumb;
	}

/////////////////////////////////////////////////////////////////////////////
//	Process Information
//	-I spent a long time trying to break this method up into different segments but it became a challenge
//	-since the connection object can't be used in other static contexts. Therefore I had to do all the
//	-processing in one static method that I could then call from the main method.	
	public static void processInformation(String loadInfo){
		//Variables to hold the database information
		String dbAccess = null;
		String user = null;
		String password = null;
		String[] userInfo = null;
		Connection connection = null;
		String tempLat = null;
		String tempLong = null;
		//Variables for origin flight information
		String originCode = null;
		String originCityName = null;
		String originCountryName = null;
		Double originLatitude = null;
		Double originLongitude = null;
		//Variables for destination flight information
		String destCode = null;
		String destCityName = null;
		String destCountryName = null;
		Double destLatitude = null;
		Double destLongitude = null;

		Integer numberOfFlights = null;

		try{
			Scanner dbInfo = new Scanner(new BufferedReader(new FileReader(loadInfo)));
			String infoLine = dbInfo.nextLine();

			//Split the user information on any number of whitespaces and set the database info
			//as string array elements
			userInfo = infoLine.split("\\s+");
			dbAccess = userInfo[0];
			user = userInfo[1];
			password = userInfo[2];
			connection = DriverManager.getConnection(dbAccess, user, password);
			//System.out.println ("Database connection established");
			
			//Set the flight number parameter for the prepared statement
			//to get the origin and destination of the entered flight number
			String flightNumberInfo = "SELECT origin, destination "
									+ "FROM flights WHERE flightNumber= ?";
			PreparedStatement flightStmt = connection.prepareStatement(flightNumberInfo);
		    flightStmt.setString(1,flightNumberInput);
		    ResultSet flightRS = flightStmt.executeQuery();

		    //Set the origin code and destination code from the result set
		    while(flightRS.next()){
		    	originCode = flightRS.getString(1);
		    	destCode = flightRS.getString(2);
	    	}

		    //Using the origin airport code obtained from the entered flight
		    //number, get the cityname,countryname,latitude and longitude
		    String selectStmt = "SELECT city_name,country_name,latitude,longitude "
									+ "FROM world_airports "
									+ "WHERE airport_code= ?";
			PreparedStatement originStmt = connection.prepareStatement(selectStmt);
			originStmt.setString(1, originCode);

			//Using the destination airport code obtained from the entered flight
			//number, get the cityname,countryname,latitude and longitude
			PreparedStatement destinationStmt = connection.prepareStatement(selectStmt);
			destinationStmt.setString(1,destCode);

			//Create a result set for the origin and destination information
			ResultSet originResult = originStmt.executeQuery();
			ResultSet destinationResult = destinationStmt.executeQuery();

			
			//Check if the airportCode used for the Origin resultset returned anything
			//If nothing was returned indicate that there was no data
			//else get the results
			if(!originResult.isBeforeFirst())
	    			System.out.println("There was no airport with an origin code of "+originCode
	    				+ " for flight number "+ flightNumberInput);
	    	else{
				while(originResult.next())
		    	{
		    		originCityName = originResult.getString(1);		//First Column(city_name)
		    	    originCountryName = originResult.getString(2);	//Second Column(country_name)
					tempLat = originResult.getString(3);			//Third Column(latitude)
		    	    tempLong = originResult.getString(4);			//Third Column(latitude)

		    	    //Print the city name and country name of the Origin
		    	    System.out.println("City of Origin: "+originCityName); 
		    	    System.out.println("Country of Origin: "+originCountryName);
		    	    System.out.println("---------------------------------------");
      
				}
				originLatitude = Double.parseDouble(tempLat);
				originLongitude = Double.parseDouble(tempLong);    
			}//End of check for the origin result is empty

			//Check if the airportCode used for the  Destination resultset returned anything
			//	-If nothing was returned indicate that there was no data
			//	-else get the results
	    	if(!destinationResult.isBeforeFirst())
	    			System.out.println("There was no airport with an destination code of "+originCode
	    				+ " for flight number "+ flightNumberInput);
	    	else{
				while(destinationResult.next())
		    	{
		    		destCityName = destinationResult.getString(1);		//First Column(city_name) 
		    	    destCountryName = destinationResult.getString(2);	//Second Column(country_name)
					tempLat = destinationResult.getString(3);			//Third Column(latitude) 
		    	    tempLong = destinationResult.getString(4);			//Fourth Column(longitude)
		    		
		    		//Print the city name and country name of the destination
					System.out.println("City of Destination: "+destCityName);
					System.out.println("Country of Destination: "+destCountryName);
					System.out.println("---------------------------------------");

					//TEST print statements
					//System.out.println("Destination: "+destinationResult.getString(3));      
					//System.out.println("Destination: "+destinationResult.getString(4));  
				}
				destLatitude = Double.parseDouble(tempLat);
				destLongitude = Double.parseDouble(tempLong);
			}//End of check for the destination result 
	  		
	  		//Create a statement to count the number of flights available between 
	  		//the origin and destination of the entered flight number
	  		//	-We must minus one flight from the result since it accounts for the flight
	  		//	-we entered and we are looking for other flights available
	  		String countStmt = "SELECT origin,destination,COUNT(flightNumber) FROM flights "
	  						+	"WHERE origin = ? AND destination = ?";
	  		PreparedStatement countFlights = connection.prepareStatement(countStmt);
	  		countFlights.setString(1,originCode);
	  		countFlights.setString(2,destCode);
	  		ResultSet countResult = countFlights.executeQuery();
	  		while(countResult.next()){
	  			numberOfFlights = Integer.parseInt(countResult.getString(3))-1;
  				System.out.println("There are "+numberOfFlights+" flights available for a flight between "
  					+ originCode +" and "+ destCode);
	  		}
	  		
	  		//Check if I can calculate the distance
			if(originLatitude != null && destLatitude != null){
				calculateDistance(originLatitude,originLongitude,destLatitude,destLongitude);
			}

       	//Exceptions
		}catch (SQLException e) { 
			e.printStackTrace(); 
        }catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error: Not able to open file " + loadInfo);
		    System.exit(1);
		// Close the connection after processing queries
		}finally{
           if (connection != null){
               try{
                   connection.close ();
                   //System.out.println ("Database connection terminated");
               }catch (Exception e) { /* ignore close errors */ }
           }
        }//End of finally
	}//End of processInformation


//////////////////////////////////////////////////////////////////////////////////
// calculateDistance
//	-Since my computation was wrong with the last program I decided to break down
//	-every step, even when converting radians to km
	public static void calculateDistance(Double originLat, Double originLong, Double destLat, Double destLong){
		Double longDiff = destLong - originLong;	
		
		double tempDist = Math.sin(deg2rad(originLat)) * Math.sin(deg2rad(destLat)) 
					+ Math.cos(deg2rad(originLat)) * Math.cos(deg2rad(destLat)) * Math.cos(deg2rad(longDiff));
  		tempDist = Math.acos(tempDist);
  		tempDist = rad2deg(tempDist);
  		//Convert the radians into miles first
  		tempDist = tempDist * 60 * 1.1515;
  		//Then convert the miles into kilometers and set as kmDistance
  		Double kmDistance = tempDist * 1.609344;

		System.out.println("---------------------------------------");
		System.out.format("The approximate distance for flight number "+flightNumberInput
				 + " is: %.0f km %n%n", kmDistance);
	}//End of calculateDistance

/////////////////////////////////////////////////////////////////////////////////////
// main
// Main method to get file from command line and continue to get users input until 
// a '0' has been entered, which will end the program
	public static void main(String[] args) {
		String inputFile = null;
		Boolean valid = false;
		// Only allow 1 parameter when running the program
		if(args.length != 1){
			System.err.println("Error: Wrong number of arguments");
			System.exit(2);
		}
		//Set the inputFile entered as a argument entered
    	inputFile = args[0]; 
    	//Continue to get users input untill a 0 is entered
    	while(true){
    		flightNumberInput = getFlightNumber();

			if(flightNumberInput.equals("0"))
				break;
			else
		    	//Process the information with the give entered flight number
				processInformation(inputFile);
		}

	}//End main

}