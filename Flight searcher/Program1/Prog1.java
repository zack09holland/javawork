/*********************************************************************
	By Zachary Holland
	CS330 Program1
		~ Prog1 utilizes the public class Airdata to construct and  
		~ calculate data in an efficient way. Tags can be entered 
		~ in either uppercase or lowercase but must be valid in 
		~ order to calculate distances between points. The number
		~ of airports per country as well as the total number of airports
		~ will still be displayed regardless of valid input.

	  Program step-by-step breakdown 
		*Scan CSV file and split data into obtainable sections
			-Put airportTag and a list of strings into HashMap
			-Put countryName, and # of occurrences into HashMap
				-Keep totalTally of airports
		* Get airportCode's from user
		* 	-always make uppercase
		* Send startDest and finalDest into a Distance method
		* 	-Output distance to get from A to B
		* Loop through countryMap 
		* 	-Output number of airports per country
		* 	-Output total number of airports
**********************************************************************/

import java.io.*;
import java.io.IOException;
import java.lang.NullPointerException;

public class Prog1 {
	
	// Main Method to run all the functions and display necessary information
	public static void main(String[] args) {
		String inputName=null;
		String departureTag=null;
		String arrivalTag=null; 

		// Only allow 1 parameter when running the program
		if(args.length != 1){
			System.err.println("Error: Wrong number of arguments");
			System.exit(2);
		}
		// Set the filename entered as a string
	    	inputName = args[0]; 

	    	//Load the airport data
	    	AirData.loadAirportData(inputName);

		// Obtain the airport tags from the user
		try{
			System.out.print("Enter departure aiport tag here : ");
	    		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	    		departureTag = bufferRead.readLine();
		
			System.out.print("Enter arrival aiport tag here : ");
	    		BufferedReader bufferRead2 = new BufferedReader(new InputStreamReader(System.in));
	    		arrivalTag = bufferRead2.readLine();
		}catch(IOException e){
			System.out.println("Seems you entered something bad, try again.");
		}
		// Calculate the distance, the tags can be uppercase or lowercase, they must be a valid tag
		// though. 
		try{
			// Output the number of airports per country and the total amount.
	    		AirData.calculateCountryNumbers();
			AirData.calculateDistance(departureTag.toUpperCase(),arrivalTag.toUpperCase());
	    	// If there is an error with the airport tag entered, raise an error message
	   	 }catch(Exception e){
	    		System.out.println("You appear to have entered an unknown aiport, try again./n");
	    	}
	    	

	}//End main

}
