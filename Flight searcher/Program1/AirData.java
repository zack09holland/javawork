import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.FileReader;
import java.lang.Math.*;

public class AirData {	
	static HashMap<String,String[]> airportMap;
	static HashMap<String,Integer> countryMap;

	// Radius of Earth obtained from google
	static Double earthRadius = 6335.44;

	///////////////////////////////////////////////////////////////////////////////	
	/* calculateDistance
		* Using the Great-circle distance equation the distance between two points
		* can be calculated given the longitude and latitude for the starting
		* destination and the final destination.
	*/
	public static void calculateDistance(String departure, String arrival) {
		String[] departureInfo = null;
		String[] arrivalInfo = null;
		Double departureLong = null;
		Double departureLat = null;
		Double arrivalLong = null;
		Double arrivalLat = null;
		Double radianLength;
		Double kmDistance;
		String tempLat;
		String tempLong;
		
		// Cycle through the map of airports and obtain the data for the specified tag
		// provided by the user.
		for (Map.Entry<String, String[]> map : airportMap.entrySet()) {
		    String key = map.getKey();
		    String[] listInfo = map.getValue();

			if(key.contentEquals(departure)){
				//Set the contents of the list into a string array for the departure flight
				departureInfo = listInfo;
				tempLat = departureInfo[5].toString();
				tempLong = departureInfo[6].toString();
				departureLat = Double.parseDouble(tempLat);
				departureLong = Double.parseDouble(tempLong);
			}		
			if(key.contentEquals(arrival)){
				// Set the contents of the list to a string array for the arrival flight
				arrivalInfo = listInfo;
				tempLat = arrivalInfo[5];
				tempLong = arrivalInfo[6];
				arrivalLat = Double.parseDouble(tempLat);
				arrivalLong = Double.parseDouble(tempLong);
			}
			
		}// End of map loop

		Double longDiff = arrivalLong - departureLong;	//Created a variable to hold the difference of the longitude
		radianLength = Math.acos(Math.sin(arrivalLat)*Math.sin(departureLat) + Math.cos(arrivalLat)*Math.cos(departureLat)*Math.cos(longDiff));
		kmDistance = earthRadius * Math.toRadians(radianLength);
		System.out.println("---------------------------------------");
		System.out.format("The approximate distance between " + departure + " and " + arrival + " is: %.0f km %n%n", kmDistance);
		return;
	}// End of calculateDistance
	///////////////////////////////////////////////////////////////////////////////




	//////////////////////////////////////////////////////////////////////////////
	/* calculateCountry method
		* Calculate the number of times each country appears in the
		* airport data list. Take each name and put it in a hashmap
		* and increment it for each occurrence.
	*/
	public static void calculateCountryNumbers(){
		String countryName;
		Integer airportTotal =0;
		//Create hashmap for the countries and the number of times they appear in the data
		countryMap = new HashMap<String, Integer>();
		//Cycle through the airport data hash map and retrieve data
		for (Map.Entry<String, String[]> map : airportMap.entrySet()) {
		    String key = map.getKey();
		    String[] listInfo = map.getValue();
		    countryName = listInfo[3].toString();
		    
		    //If the hashmap contains the country already, update it
		    if(countryMap.containsKey(countryName)){
		    	countryMap.put(countryName,countryMap.get(countryName) +1);
		    }else{
		    	countryMap.put(countryName, 1);
		    }

		}//End of map loop

		// Display total amount of aiports from data and number of airports in each country 
		for (Map.Entry<String, Integer> map : countryMap.entrySet()) {
		    String key = map.getKey();
		   	Integer value = map.getValue();
		    System.out.format( key + " has " + value + " airport(s) %n");
			airportTotal = airportTotal + value;			
		}//End of map loop
		
		System.out.println("----------------------------------");
		System.out.format("The file has %d airports listed %n",airportTotal);
		
	

	}// End of calculateCountryNumbers
	///////////////////////////////////////////////////////////////////////////////
	



	///////////////////////////////////////////////////////////////////////////////	
	/* Load the airport data
		* Take the file name as an arugment and parse the CSV file.
		* Populate the hashmap with the aiport tag as the key and
		* the rest of the information on the line into a string array,
		* for the value.
	*/
	public static void loadAirportData (String filename) {
		String inputLine;
		String airportTag;
		String[] lineInfo;
		// Create an accessible hash map for other methods
		airportMap = new HashMap<String,String[]>();
		Scanner airportData=null;
		try{
			airportData = new Scanner(new BufferedReader(new FileReader(filename)));
		} // Catch block 
		 catch (IOException e) {
		        e.printStackTrace();
		        System.out.println("Error: Not able to open file " + filename);
		        System.exit(1);
			} 
			while (airportData.hasNextLine()){
				inputLine = airportData.nextLine();
				
				// Split on the comma if that comma has zero, or an even number of quotes ahead of it.
				lineInfo = inputLine.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
				airportTag = lineInfo[0];
				
				//Populate hash map of airport tags and the line info
				airportMap.put(airportTag,lineInfo);	
			 }//End of airportData 
		return;
	}// End of loadAirportData
	///////////////////////////////////////////////////////////////////////////////
}// End of AirData class
